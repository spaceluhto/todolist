// Element selection function
const get = ((selection) => {
  const element = document.querySelector(selection)
  if (element) return element
  throw new Error(`this element does not exist or is typed incorrectly  --> (${selection})`)
})


// HTML values
const itemsContainer = get('.items-container')
const form = get('.form')
const input = get('.list-input')
const addBtn = get('.add-btn')
const inputContainer = get('.input-container')


// Listen if user have clicked on input field (clear if yes)
function placeholderCheck(target) {
  const subject = target.classList.contains('list-input')
  if (subject) {
    return input.placeholder = ''
  }
  input.placeholder = 'Enter Task'
}

// All Functionality Function
document.addEventListener('click', function (e) {
  let target = e.target
  placeholderCheck(target);
})

// FeedBack message function for fast message creation
const showMsg = ((message, boxClass) => {
  const messageBox = document.createElement('p');
  messageBox.innerHTML = `${message}`;
  messageBox.classList.add(`${boxClass}`)
  inputContainer.insertBefore(messageBox, form);
  clearFeedBack();
})
// Feedback function for letting user know and resetting input field
function feedBack(value) {
  let whiteSpaceCheck = /^\s/.test(value)
  if (!whiteSpaceCheck && value.length > 0) {
    showMsg(`<span class="value">${value}</span> was added successfully`, 'success')

  } else {
    showMsg(`Must add Something `, 'error')
  }
  return input.value = ''
}

// Delete Btn Function
function deleteTask(e) {
  const item = e.currentTarget.parentElement
  const taskText = item.textContent
  showMsg(`<span class="value">${taskText}</span> has been removed `, 'error')
  itemsContainer.removeChild(item)
}
// Complete Btn Function
function completeTask(e) {
  const item = e.currentTarget.parentElement
  const taskText = item.textContent
  showMsg(`<span class="value">${taskText}</span> has been completed `, 'success')
  itemsContainer.removeChild(item)
}



// feedback clear timer function
function clearFeedBack() {
  setTimeout(() => {
    inputContainer.querySelector('p').remove()
  }, 1500)
}

// Add Btn Function
addBtn.addEventListener('click', function (e) {
  let value = input.value
  let whiteSpaceCheck = /^\s/.test(value)
  feedBack(value);
  e.preventDefault();
  if (!whiteSpaceCheck && value.length > 0) {
    createListItem(value)
  }
})
// Creating list item format and adding to list  function
function createListItem(value) {
  // creating div 
  const div = document.createElement('div')
  div.classList.add('list-item')
  div.innerHTML = ` 
  <p class="item">${value}</p>
  <div class="delete-btn btn"><i class="fa fa-times fa-2x"></i></div>
  <div class="complete-btn btn"><i class="fa fa-check-square fa-2x"></i></div>
  `
  const deleteBtn = div.querySelector('.delete-btn')
  const completeBtn = div.querySelector('.complete-btn')
  deleteBtn.addEventListener('click', deleteTask)
  completeBtn.addEventListener('click', completeTask)

  // append container
  itemsContainer.appendChild(div)
}

